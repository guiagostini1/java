package listadeexercicios;

import java.util.Scanner;

public class Ex4 {
	
	//Programa para calcular e imprimir o n�mero de l�mpadas necess�rias para iluminar um determinado c�modo

	static float Pot_lamp, Larg_com, Comp_com, Area_com, Pot_total ;
	static int Num_lamp;
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Qual a potencia da lampada (em watts) ?");
		Pot_lamp = tecla.nextFloat();
		
		System.out.println("Qual a largura do comodo (em metros) ?");
		Larg_com = tecla.nextFloat();
		
		System.out.println("Qual o comprimento do comodo (em metros) ?");
		Comp_com = tecla.nextFloat();
		
		Area_com = Area_com();
		Pot_total = Pot_total();
		Num_lamp = Num_lamp();
		
		
		System.out.println("Numero de lampadas necessarias para iluminar esse comodo :" + Num_lamp);

	}

	public static float Area_com() {
		return Area_com = Larg_com * Comp_com;
	}
	
	public static float Pot_total () {
		return Pot_total = Area_com * 18;
	}
	
	public static int Num_lamp() {
		return Num_lamp = (int) (Pot_total / Pot_lamp);
	}
}
