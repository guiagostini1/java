package listadeexercicios;

import java.util.Scanner;

public class Sele�ao17 {
	
	//um programa para ler 2 valores inteiros e uma das seguintes opera��es a serem
	//executadas (codificada da seguinte forma: 1.Adi��o, 2.Subtra��o, 3.Divis�o, 4.Multiplica��o).

	public static void main(String[] args) {

		int A, B, Op;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Informe um valor: ");
		A = tecla.nextInt();
		
		System.out.println("Informe outro valor: ");
		B = tecla.nextInt();
		
		System.out.println("1.Adi��o  2.Subtra��o  3.Divis�ao  4.Multiplica��o ");
		Op = tecla.nextInt();
		
		
		switch (Op) {
			case 1: System.out.println("Soma: "+ (A+B)); break;
			case 2: System.out.println("Subtra��o: "+ (A-B)); break;
			case 3: System.out.println("Divis�o: "+ (A/B)); break;
			case 4: System.out.println("Multiplica��o: "+ (A*B)); break;
		}
	}

}
