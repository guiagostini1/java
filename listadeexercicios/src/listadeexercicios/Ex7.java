package listadeexercicios;

import java.util.Scanner;

public class Ex7 {

	//Programa que leia o comprimento da pista (em metros), o n�mero total de voltas a serem percorridas no 
	//grande pr�mio, o n�mero de reabastecimentos desejados e o consumo de combust�vel do carro (em Km/L). 
	//Calcular e escrever o n�mero m�nimo de litros necess�rios para percorrer at� o primeiro reabastecimento
	
	public static void main(String[] args) {

		float Comp, Consumo, Quant_min, Periodo, Percurso;
		int Volta, Reabas;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Informe o comprimento da pista (metros) :");
		Comp = tecla.nextFloat();
		
		System.out.println("Informe o nuemro de voltas a serem percorrido :");
		Volta = tecla.nextInt();
		
		System.out.println("Informe o n�mero de reabastecimentos desejados :");
		Reabas = tecla.nextInt();
		
		System.out.println("Informe o consumo de combust�vel do carro (Km/L) :");
		Consumo = tecla.nextFloat();
		
		
		Percurso = (Comp * Volta) / 1000;
		Periodo = Percurso / Reabas;
		Quant_min = Periodo / Consumo;
		
		
		System.out.println("O n�mero de litros necessarios para percorrer ate o primeiro reabastecimento � :" + Quant_min);
	}

}
