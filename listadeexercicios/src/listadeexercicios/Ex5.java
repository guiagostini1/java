package listadeexercicios;

import java.util.Scanner;

public class Ex5 {
	
	//programa para ler as dimens�es de uma cozinha retangular, calcular e escrever a quantidade de caixas 
	//de azulejos para se colocar em todas as suas paredes
	
	static float Comp, Larg, Alt, Area;
	static int Caixas;
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("Qual o comprimento da cozinha ?");
		Comp = tecla.nextFloat();
		
		System.out.println("Qual a largura da cozinha ?");
		Larg = tecla.nextFloat();
		
		System.out.println("Qual a altura da cozinha?");
		Alt = tecla.nextFloat();
		
		
		Area = Area();
		Caixas = Caixas();
		
		
		System.out.println("Quantidade de caixasde azulejos para colocar em todas as pareder :" + Caixas);	
	}
	
	public static float Area() {
		return Area = (Comp * Alt * 2) + (Larg * Alt * 2);
	}
	
	public static int Caixas() {
		return Caixas = (int) (Area / 1.5);
	}
}
