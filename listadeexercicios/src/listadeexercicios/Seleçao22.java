package listadeexercicios;

import java.util.Scanner;

public class Sele�ao22 {
	
	//programa que leia a idade de 2 homens e 2 mulheres (considere que a idade dos
	//homens ser� sempre diferente, assim como das mulheres). Calcule e escreva a soma das idades do
	//homem mais velho com a mulher mais nova, e o produto das idades do homem mais novo com a
	//mulher mais velha

	public static void main(String[] args) {
		
		int h1, h2, m1, m2;
		Scanner tecla = new Scanner (System.in);

		
		System.out.println("Idade do primeiro homem: ");
		h1 = tecla.nextInt();
		
		System.out.println("Idade do segundo homem: ");
		h2 = tecla.nextInt();
		
		System.out.println("Idade da primeira mulher: ");
		m1 = tecla.nextInt();
		
		System.out.println("Idade da segunda melher: ");
		m2 = tecla.nextInt();
		
		
		if ((h1 > h2) && (m1 > m2)) {
			System.out.println("Homem mais velho + mulher mais nova = " + (h1+m1));
			System.out.println("Homem mais novo * mulher mais velha = " + (h2*m2));
		}
		else {
			if((h1 < h2) && (m1 < m2)) {
				System.out.println("Homem mais velho + mulher mais nova = " + (h1+m2));
				System.out.println("Homem mais novo * mulher mais velha = " + (h2*m1));
			}
			else {
				System.out.println("Homem mais velho + mulher mais nova = " + (h2+m2));
				System.out.println("Homem mais novo * mulher mais velha = " + (h1*m1));
			}
		}
	}

}
