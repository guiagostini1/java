package listadeexercicios;

import java.util.Scanner;

public class Ex3 {
	
	// Converter Celsius para Fahrenheit
	
	static float Temp_c;
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("Informe a temperatura em graus Celsius :");
		Temp_c = tecla.nextFloat();
		
		Temp_c = Temp_c();
		
		System.out.println("A temperatura em Fahrenheit:" + Temp_c);

	}

	public static float Temp_c() {
		return ((Temp_c * 9)/ 5) + 32;
	}
}
