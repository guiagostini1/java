package listadeexercicios;

import java.util.Scanner;

public class Repeti�ao9 {
	
	//Programa para ler 2 notas de um aluno, calcular e imprimir a m�dia final
	// perguntar se quer calcular de novo
	// mostrar numero de aprovado, reprovado e total de alunos
	
	public static void main(String[] args) {
		
		float n1, n2, Media;
		int Resp, ContApro = 0, TotalAlunos = 0, ContRepro = 0;
		Scanner tecla = new Scanner (System.in);

		do {
			System.out.println("Digite a nota da primeira avalia��o: ");
			n1 = tecla.nextFloat();
		
			System.out.println("Digite a nota da segunda avalaia��o: ");
			n2 = tecla.nextFloat();
		
			TotalAlunos = TotalAlunos + 1;
			Media = (float) (n1 + n2) / 2;
			System.out.println("A media do aluno �: " + Media);
			
			if (Media >= 6) {
				ContApro = ContApro + 1;
			}
			
			if (Media < 6) {
				ContRepro = ContRepro + 1;
			}
		
			System.out.println("Calcular a media de outro aluno? 1.Sim  2.N�o ");
			Resp = tecla.nextInt();
		
		} while (Resp == 1);
		
		System.out.println("Quantidade de alunos aprovados: " + ContApro);
		System.out.println("Quantidade de alunos reprovados: " + ContRepro);
		System.out.println("Quantidade de alunos: " + TotalAlunos);
		
	}

}
