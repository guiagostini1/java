package listadeexercicios;

import java.util.Scanner;

public class Repeti�ao1 {
	
	//Programa para ler 2 valores e imprimir 
	//o resultado da divis�o do primeiro pelo segundo e que seja
	// diferente de 0

	public static void main(String[] args) {
		
		int a, b;
		float Divisao;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Digite um valor inteiro: ");
		a = tecla.nextInt();
		
		
		do {
			System.out.println("Digite outro valor inteiro: ");
			b = tecla.nextInt();
			if (b == 0) {
				System.out.println("Valor invalido! ");
			}
		} while (b == 0);
		
		
		Divisao = a/b;
		System.out.println("A divis�o do primeiro valor pelo segundo �: "+ Divisao);
	}

}
