package listadeexercicios;

import java.util.Scanner;

public class Sele�ao21 {
	
	//programa que leia as medidas dos lados de um tri�ngulo e escreva se ele �
	//Equil�tero, Is�sceles ou Escaleno

	public static void main(String[] args) {
		
		float x, y, z;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Informe a medida 1: ");
		x = tecla.nextFloat();
		
		System.out.println("Informe a medida 2: ");
		y = tecla.nextFloat();
		
		System.out.println("Informe a medida 3: ");
		z = tecla.nextFloat();
		
		
		if ((x == y) && (y == z)) {
			System.out.println("Equilatero");
		}
		else {
			if ((x == y) || (y == z) && (x == z)) {
				System.out.println("Isosceles");
			}
			else {
				if ((x != y) && (y != z) && (x != z)) {
					System.out.println("Escaleno");
				}
			}
		}
	}

}
