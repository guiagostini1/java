package listadeexercicios;

import java.util.Scanner;

public class Sele�ao23 {
	
	//programa que leia o valor de 3 �ngulos de um tri�ngulo e escreva se o tri�ngulo �
	//Acut�ngulo, Ret�ngulo ou Obtus�ngulo

	public static void main(String[] args) {
		
		float a, b, c;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Primeiro angulo: ");
		a = tecla.nextFloat();
		
		System.out.println("Segundo angulo: ");
		b = tecla.nextFloat();
		
		System.out.println("Terceiro angulo: ");
		c = tecla.nextFloat();
		
		
		if ((a == 90) || (b == 90) || (c == 90)) {
			System.out.println("Retangulo...");
		}
		else {
			if ((a > 90) || (b > 90) || (c > 90)) {
				System.out.println("Obtusangulo...");
			}
			else {
				System.out.println("Acutangulo...");
			}
		}

	}

}
