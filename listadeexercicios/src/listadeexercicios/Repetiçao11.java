package listadeexercicios;

import java.util.Scanner;

public class Repeti�ao11 {
	
	//Programa que verifique a validade de uma senha fornecida pelo usu�rio. 
	//A senha v�lida � o n�mero 1234
	//Se a senha informada pelo usu�rio for inv�lida, a mensagem "ACESSO NEGADO"
	//deve ser impressa e repetida a solicita��o de uma nova senha at� que ela seja v�lida

	public static void main(String[] args) {
		
		int Senha;
		Scanner tecla = new Scanner (System.in);
		
		do {
		System.out.println("Digite a senha (quatro digitos) :");
		Senha = tecla.nextInt();
		
		
		if (Senha != 1234) {
			System.out.println("Acesso Negado !");
		}		

		} while (Senha != 1234);
			
		System.out.println("Acesso Permitido! ");	
	}

}
