package listadeexercicios;

import java.util.Scanner;

public class Sele�ao13 {
	
	//Programa para ler um n�mero inteiro 
	// e escrever se � par ou �mpar.

	public static void main(String[] args) {

		int Num;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Digite um numero : ");
		Num = tecla.nextInt();
		
		
		if (Num %2 == 0) {
			System.out.println("O numero digitado � PAR.");
		}else {
			System.out.println("O numero digitado � IMPAR.");
		}
	}

}
