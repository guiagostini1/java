package listadeexercicios;

import java.util.Scanner;

public class Sele�ao3 {
	
	//Programa para ler as notas das duas avalia��es de um aluno no semestre, calcular e
	//escrever a m�dia semestral e a seguinte mensagem: PARAB�NS! Voc� foi aprovado!

	public static void main(String[] args) {
		
		float Nota1, Nota2, Media;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Digite a nota da primeira avalia��o : ");
		Nota1 = tecla.nextFloat();
		
		System.out.println("Digite a nota da segunda avalia��o : ");
		Nota2 = tecla.nextFloat();
		
		
		Media = (Nota1 + Nota2) / 2;
		if (Media >= 6.0) {
			System.out.println("Parabens, voc� foi aprovado");
		}
		else{
			System.out.println("Reprovado");
			}
	}

}
