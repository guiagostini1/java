package listadeexercicios;

import java.util.Scanner;

public class Ex6 {
	//Calcular o rendimento de seu carro

	public static void main(String[] args) {
		
		float Odom_i, Odom_f, Valor_t, Media, Litros ,Lucro, Gasol_l = (float) 1.90;
		Scanner tecla = new Scanner (System.in);

		
		System.out.println("Marca��o inicial do odometro (Km) :");
		Odom_i = tecla.nextFloat();
		
		System.out.println("Marca��o final do odometro (Km) :");
		Odom_f = tecla.nextFloat();
		
		System.out.println("Quantidade de combustivel gasto (litros) :");
		Litros = tecla.nextFloat();
		
		System.out.println("Valor total do recibo (R$) : ");
		Valor_t = tecla.nextFloat();
		
		
		Media = (Odom_f - Odom_i) / Litros;
		Lucro = Valor_t - (Litros * Gasol_l);
		
		
		System.out.println("Media de consumo em Km/L : " + Media);
		
		System.out.println("Lucro (liquido) do dia : R$" + Lucro);
		
	}

}
