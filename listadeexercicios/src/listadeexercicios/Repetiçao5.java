package listadeexercicios;

import java.util.Scanner;

public class Repeti�ao5 {

	//Programa para ler as notas da 1� e 2� avalia��es de um aluno, calcular e imprimir a
	//m�dia semestral. Fa�a com que o algoritmo s� aceite notas v�lidas
	//Deve ser impressa a mensagem "Nota inv�lida" caso a nota informada n�o perten�a ao intervalo
	//e se quer fazer uma nova consulta
	
	public static void main(String[] args) {
		
		int n1, n2, NovoCal=0;
		float Media;
		Scanner tecla = new Scanner (System.in);
		
		do {
		do {
			System.out.println("Informe a primeira nota: ");
			n1 = tecla.nextInt();
			
			System.out.println("Informe a segunda nota: ");
			n2 = tecla.nextInt();
			
			if((n1 < 0) || (n1 > 10) || (n2 < 0) || (n2 > 10)) {
				System.out.println("Nota invalida! ");
			}
		} while ((n1 < 0) || (n1 > 10) || (n2 < 0) || (n2 > 10));
		
		
		Media = (float) (n1 + n2) / 2;
		System.out.println("Media: " + Media);
		
		System.out.println("Quer fazer outro calculo ? 1.Sim  2.Nao");
		NovoCal = tecla.nextInt();
		
	} while (NovoCal == 1);
	}

}
