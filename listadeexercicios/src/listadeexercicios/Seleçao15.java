package listadeexercicios;

import java.util.Scanner;

public class Sele�ao15 {
	
	//Programa para ler o n�mero de lados de um pol�gono regular e a medida do lado

	public static void main(String[] args) {

		int Num_lado;
		float Med_lado;
		Scanner tecla = new Scanner (System.in);
		
		
		System.out.println("Quantos lados tem o poligono : ");
		Num_lado = tecla.nextInt();
		
		System.out.println("Qual a medida do lado (cm) : ");
		Med_lado = tecla.nextFloat();
		
		
		if (Num_lado == 3) {
			System.out.println("Triangulo");
			System.out.println("Perimetro:  "+ Med_lado * 3);
		}else {
			if (Num_lado == 4 ) {
				System.out.println("Quadrado");
				System.out.println("Area: "+ Med_lado * Med_lado);
			}else {
				if (Num_lado == 5) {
				System.out.println("Pentagono");
			}
			}
		}
		
		if(Num_lado < 3) {
			System.out.println("Nao � um poligono");
		} else {
			if (Num_lado > 5) {
				System.out.println("Poligono n�o identificado");
			}
		}
	}

}
